## Acid Labs ruby interview test

We are glad that you came this far of the process, the following test have been made to evaluate your solve problem skills.

Be aware that, in order to take in account your submission it have to be sended in your own repository link to the solution.

The following code decides which promotion will be rendered along the price of a product, the competition between the different promotions that the product is in obey the following rules:

A product have two attributes, a **name** and a **price** and a 0 to infinite promotions.

A promotion have a **name**, **discount**, **frequency** and **kind**.

Promotions:
- if a promotion is of **kind** percentage, it will discount the respective percentage on the product's price
- if a promotion is of **kind** fixed, it will discount the respective fixed amount on the product's price
- if a promotion is of **kind** N\*M, it will show the discount on the amount of the product's if N products have been selected at the price of M products, an example of this case is an offer **2x1**, for every 2 products an product will be reduced
- if a promotion is of **kind** N-unit-percentage, it will discount the respective percentage on the N product, an example of this case is an offer **40% on 2nd** it will discount 40% of the price of the second product.

The third promotion have an error and the last promotion is not implemented, you have to do it.

This rules are applied in the showroom file, which makes to compete the promotions and show the winner along with the details of the products.

Making requirement clear, a product can't have a 0 nor negative price values.

We are going to evaluate the solution on this topics:

- An scalable solutions in which it will be easy to create a new kind of promotion
- An concise and clear git process
- 100% code coverage
- Clear code implementation and separation of concern
- Clear code implementation and separation of concerns

You have to provide instructions to run the test suite and the program. You are free to choose the language it suites you the best, if you want to use a different programming language it will be needed a dockerized program and provide the commands to run it.

Use the following code as seed data to test the functionality.

```ruby
promo1 = Promotion.new('50%', 50, nil, 'percentage')
promo2 = Promotion.new('2000', 2000, nil, 'fixed')
promo3 = Promotion.new('3x2', '100', 3, 'N_M')

product = Product.new('MyAwesomeProduct', 10_000, [promo1, promo2, promo3])

puts "Producto #{product.name}, precio: #{product.price}, descuento: #{product.discount}"
```

**Note:** Don't worry if you can't finish the test, this test was made to be difficult to solve and subjective, we are evaluating the process of solving a problem.
