Dir['./src/*.rb'].each { |file| require file }

promotion_1 = Promotion.new('30%', 30.0, nil, 'percentage')
promotion_2 = Promotion.new('60%', 60.0, nil, 'percentage')
promotion_3 = Promotion.new('3000', 3000, nil, 'fixed')
promotion_4 = Promotion.new('30000', 30_000, nil, 'fixed')
promotion_5 = Promotion.new('2x1', 100.0, 2, 'N_M')
promotion_6 = Promotion.new('3x1', 200.0, 3, 'N_M')
promotion_7 = Promotion.new('40% 2da unidad', 40.0, 2, 'N-unit-percentage')
promotion_8 = Promotion.new('90% 2da unidad', 90.0, 2, 'N-unit-percentage')
promotion_9 = Promotion.new('3x2', 100.0, 3, 'N_M')

product_1 = Product.new('Product1', 10_000, [promotion_1, promotion_5, promotion_6, promotion_9])
product_2 = Product.new('Product2', 100_000, [promotion_2, promotion_8, promotion_5, promotion_3])
product_3 = Product.new('Product3', 44_000, [promotion_1, promotion_4, promotion_7, promotion_2])


puts "---------------Productos-------------"
puts "Nombre,   Precio,   Descuento"
puts "Producto: #{product_1.name}, price: #{product_1.price}, descuento: #{product_1.discount}"
puts "Producto: #{product_2.name}, price: #{product_2.price}, descuento: #{product_2.discount}"
puts "Producto: #{product_3.name}, price: #{product_3.price}, descuento: #{product_3.discount}"

# Expected result
=begin
  --------------Productos------------------
  Producto: Product1, price: 10000, descuento: 3x1
  Producto: Product2, price: 100000, descuento: 60%
  Producto: Product3, price: 44000, descuento: 30000
=end
