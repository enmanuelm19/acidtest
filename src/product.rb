require 'pry'
class Product
  attr_accessor :name, :price, :promotions

  def initialize(name, price, promotions)
    @name = name
    @price = price
    @promotions = promotions
  end

  def discount
    discount_total = 0
    discounts = []
    discount_name = ''
    frequencies = []
    @promotions.each { |p| frequencies <<  p.frequency.to_i }
    top_frequency = frequencies.sort.last
    @promotions.each do |promotion|
      if promotion.kind === 'percentage'
        discount = (@price * (promotion.discount / 100)) * top_frequency
        discounts << { name: promotion.name, discount: discount }
      elsif promotion.kind === 'fixed'
        discount = promotion.discount * top_frequency
        discounts << { name: promotion.name, discount: discount }
      elsif promotion.kind === 'N_M'
        discount = (@price * (promotion.discount / 100))
        discounts << { name: promotion.name, discount: discount }
      elsif promotion.kind === 'N-unit-percentage'
        discount = 0
        discounts << { name: promotion.name, discount: discount }
      end
    end
    discounts.each do |d|
      if d[:discount] > discount_total
        discount_total = d[:discount]
        discount_name = d[:name]
      end
    end
    discount_name
  end
end
