class Promotion
  attr_accessor :name, :discount, :frequency, :kind

  def initialize(name, discount, frequency, kind)
    @name = name
    @discount = discount
    @frequency = frequency
    @kind = kind
  end
end
